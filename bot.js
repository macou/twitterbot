var twitter = require('./twitter.js');
var quotes = require('./quotes.js');


var minutes = 0.1;
var tout = function(mins) {
    return 60 * mins;
}; // X seconds * mins
var interval = tout(minutes);
var counter = interval;
var startHour = 8;
var endHour = 19;

function loop() {
    setInterval(function() {
        counter--;

        if (counter % 1 === 0) {
           // writeToConsole(counter.toString());
       }

       if (counter === 0) {

        var randomNo = randomIntBetweenVals(1, 3)

        if (getCurrentHour() < startHour || getCurrentHour() > endHour) {
            randomNo = 5;
        }

        switch (randomNo) {
            case 1:
            //unfollow();
            break;
            case 2:
            followSomeone();
            break;
            case 3:
            searchAndRetweet('cycling');
            break;
            case 4:
            // localQuote();
            break;
            case 5:
            console.log("\nDoing nothing this time. Zzzzz....")
            break;
            case 6:
            console.log("\nMarcus is sleeping, so no tweeting!");
            break;
        }
    }

    if (counter === 0) {

        interval = tout(randomIntBetweenVals(5, 25));

        counter = interval;
    }

}, 1000)

};

function writeToConsole(val) {
    // process.stdout.clearLine(); // clear current text
    // process.stdout.cursorTo(0); // move cursor to beginning of line
    // process.stdout.write(counter.toString());
    console.log("\r\n" + val);
}

// tweet a inspiration quote
function localQuote() {
    var qu = quotes.getRandomLocalQuote(function(data) {

        if (data && data.length < 141) {
            twitter.tweet(data);
            console.log("\nTweeted local quote: " + data);
        } else {
            console.log("\nCouldn't tweet local quote as it was too long! " + data.length)
        }
    })
};

// follow a random friend of someone I follow
function followSomeone() {
    var follow = twitter.mingle(function() {
        console.log('\nFollowed someone, yipee!');
    })
};

function unfollow(){
    var unfriend = twitter.unfollow(function() {
        console.log('\nUnfollowed someone...  :( ');
    })
}

function searchAndRetweet(subject) {

    var retweet = twitter.search(subject, function(data) {
        //console.log(data);
        // check for data first
        if(data != undefined && data.statuses[0] != undefined)
        {
            var tweet = data.statuses[0].id_str;
            if (data.statuses[0].lang === 'en') { // only retweet english tweets
                twitter.retweet(tweet, function() {
                    console.log("\nretweeted tweet id " + tweet + " based on the subject term " + subject);
                })
            }
        }
});

}

function randomIntBetweenVals(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function getCurrentHour() {
    var now = new Date();
    var hour = now.getHours();
    return parseInt(hour);
}


var bot = {
    begin: function() {
        console.log("Bot has started...")
        loop();
    }
}

module.exports = bot;