var http = require('http');
var jsonQuotes = require('./data/quotes.json');

var quotes = {

    getRonSwansonQuote: function(callback) {
        var options = {
            host: 'ron-swanson-quotes.herokuapp.com',
            path: '/quotes',
            headers:{'Cache-Control': 'no-cache'}
        };

        callfinshed = function(response) {
            var str = '';

            //another chunk of data has been recieved, so append it to `str`
            response.on('data', function(chunk) {
                str += chunk;
            });

            //the whole response has been recieved, so we just print it out here
            response.on('end', function() {

                var q = JSON.parse(str);
                var quote = q.quote + " #RonSwanson";
                //logger.info(str);
                callback(quote);
            });
        }

        http.request(options, callfinshed).end();

    },

    getInspirationalQuote: function(callback) {
        var options = {
            host: 'notableandquotable.com/',
            path: '/RandomQuote.asmx/GetRandomQuote',
            headers:{'Cache-Control': 'no-cache'}
        };

        callfinshed = function(response) {
            var str = '';

            //another chunk of data has been recieved, so append it to `str`
            response.on('data', function(chunk) {
                str += chunk;
            });

            //the whole response has been recieved, so we just print it out here
            response.on('end', function() {

                console.log(str);

                // var q = JSON.parse(str);
                // var quote = q.contents.quote + " - " + q.contents.author;
                // //logger.info(str);
                // callback(quote);
            });
        }

        http.request(options, callfinshed).end();

    },
    quotesOnDesign: function(callback){
        var options = {
            host: 'quotesondesign.com',
            path: '/api/3.0/api-3.0.json',
            headers:{'Cache-Control': 'no-cache'}
        };

        callfinshed = function(response) {
            var str = '';

            //another chunk of data has been recieved, so append it to `str`
            response.on('data', function(chunk) {
                str += chunk;
            });

            //the whole response has been recieved, so we just print it out here
            response.on('end', function() {
                var q = JSON.parse(str);
                var quote = q.quote + " - " + q.author;
                
                //logger.info(str);
                callback(quote);
            });
        }

        http.request(options, callfinshed).end();

    },
    getRandomLocalQuote: function(callback){

            var q = jsonQuotes[randomIntBetweenVals(1, 76000)];

            callback(q.QUOTE + " - " + q.AUTHOR);

    }



}

function randomIntBetweenVals(min, max) {

    return Math.floor(Math.random() * (max - min + 1) + min);
}


module.exports = quotes;